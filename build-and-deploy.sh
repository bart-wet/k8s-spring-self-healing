#!/bin/bash

# variable name to run kubectl for minicube. Change this to suite your kubernetes deployment
kubectl='minikube kubectl --'

# Compile Java code and create runnable Jar
mvn clean install

# Build docker container and push it to the local repository
docker build -t wet-spring-k8s-demo .

# Remove possible old deployment and service
$kubectl delete deployment wet-spring-k8s-demo
$kubectl delete service wet-spring-k8s-demo

# Deploy the image from the local repository on the kubernetes cluster
$kubectl apply -f k8s-deployment.yaml

# Deploy the service on the kubernetes cluster
$kubectl apply -f k8s-service.yaml

# Wait for the whole deployment to complete before setting up the port-forwarding
sleep 15

# Setup port-forwarding to the service from outside the cluster
$kubectl port-forward service/wet-spring-k8s-demo 8080:8080