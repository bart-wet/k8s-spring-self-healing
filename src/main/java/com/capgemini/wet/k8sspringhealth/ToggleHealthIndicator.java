package com.capgemini.wet.k8sspringhealth;

import org.springframework.boot.actuate.health.Health;
import org.springframework.boot.actuate.health.HealthIndicator;
import org.springframework.stereotype.Component;

@Component
public class ToggleHealthIndicator implements HealthIndicator {

    private boolean unhealthy = false;

    @Override
    public Health health() {
        return unhealthy ?
                Health.outOfService().build() :
                Health.up().build();
    }

    public void setUnhealthy(final boolean unhealthy) {
        this.unhealthy = unhealthy;
    }

}
