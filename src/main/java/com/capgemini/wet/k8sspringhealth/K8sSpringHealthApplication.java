package com.capgemini.wet.k8sspringhealth;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class K8sSpringHealthApplication {

	public static void main(String[] args) {
		SpringApplication.run(K8sSpringHealthApplication.class, args);
	}

}
