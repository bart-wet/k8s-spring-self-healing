package com.capgemini.wet.k8sspringhealth;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class DemoController {

    private final ToggleHealthIndicator healthToggle;

    public DemoController(final ToggleHealthIndicator healthToggle) {
        this.healthToggle = healthToggle;
    }

    @GetMapping("/unhealthy/")
    public String unhealthy() {
        healthToggle.setUnhealthy(true);
        return "ok";
    }

}
