k8s Spring Self Healing Application
-----------------------------------

This is a demo project of a minimal setup for a self-healing application based on Spring Boot, Spring Actuator and 
Kubernetes. It features a simple end-point to put the application out-of-service. As this is a simple demo project,
it is far from production ready.

To fully run this project you will need the following tooling set up:
- Maven
- Kubernetes cluster (tested with minikube)
  
This demo project's deployment doesn't include and ingress rules or full service setups. It uses a simple 
port-forwarding to expose the application to the "outside world". 

   